// @flow
import React, { Component } from 'react';
import Home from '../components/Admin';

export default class HomePage extends Component {
  render() {
    return (
      <Home />
    );
  }
}
