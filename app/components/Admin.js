// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Window from './Window';
import MainMenu from './MainMenu';

import '../styles/ui.scss';

export default class Home extends Component {
  render() {
    return (
      <Window>
        <MainMenu/>
      </Window>
    );
  }
}
