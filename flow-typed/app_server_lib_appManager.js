/* @flow */

import type { Logger as Winston } from 'winston';

declare type Logger = Winston;
declare type Context = { [string]: any };
declare type RuntimeCheck = (Context) => boolean;
declare type RuntimeMethod = (Context) => Promise<Context>

declare class Module {
  constructor(string, RuntimeCheck | void, RuntimeMethod | void, RuntimeMethod | void): Module,
  isReadyToLaunch(Context): boolean,
  isRunning(): boolean,
  startup(Context): Promise<Context>,
  shutdown(Context): Promise<Context>,
  name: string
}

declare class AppManager {
  constructor(Logger): AppManager,
  addModule(Module): AppManager,
  startup(): AppManager,
  shutdown(): void
}
